# Instruction

## 1. Make database repo

```
cd htdocs/
composer create-project laravel/laravel SimpleGacha
cd SimpleGacha
touch storage/simple-gacha.sqlite
```
## 2. Migrate & Seeds

```
#!php

php artisan migrate
composer dump-autoload
php artisan db:seed
```