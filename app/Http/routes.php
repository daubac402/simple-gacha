<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::get('inventory', 'InventoryController@index');

Route::post('home/draw_gacha_1', 'HomeController@draw_normal_gacha');

Route::post('home/draw_gacha_2', 'HomeController@draw_expensive_gacha');

Route::post('home/draw_gacha_3', 'HomeController@draw_box_gacha');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
