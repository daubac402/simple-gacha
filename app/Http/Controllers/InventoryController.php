<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserItem;
use App\ItemMaster;
use App\Services\AppConstants;
use Illuminate\Support\Facades\Auth;
use App\Services\LogicUserCoin;

class InventoryController extends Controller {

	private $logic_user_coin;
	private $item_masters;
	private $user_items;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->item_masters = ItemMaster::all();
		$this->user_items = UserItem::all();
		$this->_append_item_data();
		$this->logic_user_coin = new LogicUserCoin();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_current_coin = $this->logic_user_coin->get_current_user_coin(Auth::user());

		$data['user_current_coin'] = $user_current_coin;
		$data['user_items'] = $this->user_items;
		return view('inventory', $data);
	}

	private function _append_item_data()
	{
		foreach ($this->user_items as &$user_item) {
			foreach ($this->item_masters as $item_master) {
				if ($user_item->item_id == $item_master->id)
				{
					$user_item->name = $item_master->name;
					break;
				}
			}
		}
	}

}
