<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\GachaMasterType;
use App\Services\LogicOddGacha;
use App\Services\LogicBoxGacha;
use App\Services\LogicUserCoin;

class HomeController extends Controller {

	private $gacha_master_types;
	private $logic_odd_gacha;
	private $logic_box_gacha;
	private $logic_user_coin;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->gacha_master_types = GachaMasterType::all();
		$this->logic_odd_gacha = new LogicOddGacha();
		$this->logic_box_gacha = new LogicBoxGacha();
		$this->logic_user_coin = new LogicUserCoin();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_current_coin = $this->logic_user_coin->get_current_user_coin(Auth::user());

		$data['gacha_master_types'] = $this->gacha_master_types;
		$data['user_current_coin'] = $user_current_coin;
		return view('home', $data);
	}

	public function draw_normal_gacha() {
		$gacha_master_type = $this->gacha_master_types[0];
		$gacha_result = $this->logic_odd_gacha->draw_gacha(Auth::user(), $gacha_master_type);
		
		$user_current_coin_update = NULL;
		if ($gacha_result['error'] != LogicOddGacha::SUCCESS)
		{
			switch ($gacha_result['error']) {
				case LogicOddGacha::ERROR_OUT_OF_MONEY:
					$message = 'ERROR! You do not have enough coin.';
					break;

				default:
					$message = 'Unknown Error!';
					break;
			}
		}
		else
		{
			if ($gacha_result['draw_free'])
			{
				$message = "You got item: {$gacha_result->item_id} free";
			}
			else
			{
				$user_current_coin_update = $this->logic_user_coin->get_current_user_coin(Auth::user());
				$message = "You got item: {$gacha_result->item_id} with {$gacha_master_type->price} coins";
			}
		}
		

		$json_data = array(
			'message' 					=> $message,
			'user_current_coin_update'	=> $user_current_coin_update,
		);
		echo json_encode($json_data);
	}

	public function draw_expensive_gacha() {
		$gacha_master_type = $this->gacha_master_types[1];
		$gacha_result = $this->logic_odd_gacha->draw_gacha(Auth::user(), $gacha_master_type);
		
		$user_current_coin_update = NULL;
		if ($gacha_result['error'] != LogicOddGacha::SUCCESS)
		{
			switch ($gacha_result['error']) {
				case LogicOddGacha::ERROR_OUT_OF_MONEY:
					$message = 'ERROR! You do not have enough coin.';
					break;

				default:
					$message = 'Unknown Error!';
					break;
			}
		}
		else
		{
			if ($gacha_result['draw_free'])
			{
				$message = "You got item: {$gacha_result->item_id} free";
			}
			else
			{
				$user_current_coin_update = $this->logic_user_coin->get_current_user_coin(Auth::user());
				$message = "You got item: {$gacha_result->item_id} with {$gacha_master_type->price} coins";
			}
		}
		

		$json_data = array(
			'message' 					=> $message,
			'user_current_coin_update'	=> $user_current_coin_update,
		);
		echo json_encode($json_data);
	}

	public function draw_box_gacha() {
		$gacha_master_type = $this->gacha_master_types[2];
		$gacha_result = $this->logic_box_gacha->draw_gacha(Auth::user(), $gacha_master_type);
		
		$user_current_coin_update = NULL;
		$box_gacha_desc = NULL;

		if ($gacha_result['error'] != LogicBoxGacha::SUCCESS)
		{
			switch ($gacha_result['error']) {
				case LogicBoxGacha::ERROR_OUT_OF_MONEY:
					$message = 'ERROR! You do not have enough coin.';
					break;

				case LogicBoxGacha::ERROR_OUT_OF_BOX_GACHA:
					$message = 'Outch! There is nothing in box gacha.';
					break;

				default:
					$message = 'Unknown Error!';
					break;
			}
		}
		else
		{
			$user_current_coin_update = $this->logic_user_coin->get_current_user_coin(Auth::user());
			$message = "You got item: {$gacha_result->item_id} with {$gacha_master_type->price} coins";

			$box_info = $gacha_result['box_gacha_info'];
			if ($box_info)
			{
				$box_gacha_desc = "Common {$box_info->common_remain} items - Uncommon {$box_info->uncommon_remain} items - Rare {$box_info->rare_remain} items - Super Rare {$box_info->srare_remain} items";
			}
		}

		$json_data = array(
			'message' 					=> $message,
			'user_current_coin_update'	=> $user_current_coin_update,
			'box_gacha_desc'			=> $box_gacha_desc,
		);
		echo json_encode($json_data);
	}
}
