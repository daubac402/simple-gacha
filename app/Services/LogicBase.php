<?php namespace App\Services;

use App\Services\AppConstants;

class LogicBase {

	protected function get_diff_second_from_now($past_time_str)
	{
		$current_timestamp = strtotime(date('Y-m-d H:i:s'));
		$past_time = strtotime($past_time_str);
		return $current_timestamp - $past_time;
	}

	// 10h20'12" -> timestr = '102012'
	protected function get_timestamp_from_today_time_str($time_str)
	{
		return strtotime("Today $time_str");
	}
}
