<?php namespace App\Services;

use App\Services\AppConstants;
use App\Services\LogicBase;
use App\User;

class LogicUserCoin extends LogicBase {

	public function get_current_user_coin($user)
	{
		$diffsecond = $this->get_diff_second_from_now($user->coin_updated_at);
		return $user->coin + $diffsecond * AppConstants::RESTORE_COIN_EACH_SECOND;
	}

	public function take_user_coin($user, $take_amount = 0)
	{
		if (NULL == $user)
		{
			return false;
		}
		$current_user_coin = $this->get_current_user_coin($user);
		if ($current_user_coin < $take_amount)
		{
			return false;
		}
		else
		{
			$new_coin_value = $current_user_coin - $take_amount;
			return User::update_user_coin($user, $new_coin_value);
		}
	}

}
