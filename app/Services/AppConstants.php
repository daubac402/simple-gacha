<?php namespace App\Services;

class AppConstants {

	const START_COIN_AMOUNT 		= 5000;
	const RESTORE_COIN_EACH_SECOND	= 1;

	const ITEM_TYPE_COMMON			= 1;
	const ITEM_TYPE_UNCOMMON		= 2;
	const ITEM_TYPE_RARE			= 3;
	const ITEM_TYPE_SRARE			= 4;
	const ITEM_TYPES 				= array(
										self::ITEM_TYPE_COMMON, 
										self::ITEM_TYPE_UNCOMMON, 
										self::ITEM_TYPE_RARE, 
										self::ITEM_TYPE_SRARE
										);

}
