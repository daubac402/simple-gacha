<?php namespace App\Services;

use App\Services\LogicGachaBase;
use App\GachaMasterType;
use App\UserBoxGacha;
use App\Services\AppConstants;

class LogicBoxGacha extends LogicGachaBase{

	protected function make_lot_list($user, $gacha_master_type)
	{
		$lot_types_flip = array_flip(GachaMasterType::LOT_TYPES);
		$user_box_gacha = UserBoxGacha::find_by_keys($user->id, $gacha_master_type->gacha_type_id);
		if ($this-> is_reset_box_gacha($user_box_gacha, $gacha_master_type))
		{
			if (NULL == $user_box_gacha)
			{
				//create new user_box_gacha
				$user_box_gacha = new UserBoxGacha(array(
					'user_id'		=> $user->id,
					'gacha_type_id'	=> $gacha_master_type->gacha_type_id,
					'common_remain'		=> $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_COMMON]],
					'uncommon_remain'	=> $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_UNCOMMON]],
					'rare_remain'		=> $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_RARE]],
					'srare_remain'		=> $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_SRARE]],
				));
				$user_box_gacha->save();
			}
			else
			{
				//reset user_box_gacha
				$user_box_gacha->common_remain = $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_COMMON]];
				$user_box_gacha->uncommon_remain = $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_UNCOMMON]];
				$user_box_gacha->rare_remain = $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_RARE]];
				$user_box_gacha->srare_remain = $gacha_master_type['attributes'][$lot_types_flip[AppConstants::ITEM_TYPE_SRARE]];
				UserBoxGacha::update_one($user_box_gacha);
			}
			
			$lot_list = array_intersect_key($gacha_master_type['attributes'], array_flip(array_keys(GachaMasterType::LOT_TYPES)));
			return $lot_list;
		}
		else
		{
			$lot_list[$lot_types_flip[AppConstants::ITEM_TYPE_COMMON]] = $user_box_gacha->common_remain;
			$lot_list[$lot_types_flip[AppConstants::ITEM_TYPE_UNCOMMON]] = $user_box_gacha->uncommon_remain;
			$lot_list[$lot_types_flip[AppConstants::ITEM_TYPE_RARE]] = $user_box_gacha->rare_remain;
			$lot_list[$lot_types_flip[AppConstants::ITEM_TYPE_SRARE]] = $user_box_gacha->srare_remain;
			return $lot_list;
		}
	}

	protected function is_reset_box_gacha($user_box_gacha, $gacha_master_type)
	{
		if (NULL == $user_box_gacha)
		{
			return true;
		}
		$reset_timestamp = $this->get_timestamp_from_today_time_str($gacha_master_type->reset_box_gacha_time);
		$draw_timestamp = strtotime($user_box_gacha->draw_time);
		return ($draw_timestamp < $reset_timestamp);
	}

	protected function update_remain_box_gacha($user, $gacha_master_type, $lot_result)
	{
		$item_rarity_result = GachaMasterType::LOT_TYPES[$lot_result];

		$user_box_gacha = UserBoxGacha::find_by_keys($user->id, $gacha_master_type->gacha_type_id);
		if (NULL == $user_box_gacha)
		{
			return NULL;
		}

		switch ($item_rarity_result) {
			case AppConstants::ITEM_TYPE_COMMON:
				$user_box_gacha->common_remain--;
				break;
			case AppConstants::ITEM_TYPE_UNCOMMON:
				$user_box_gacha->uncommon_remain--;
				break;
			case AppConstants::ITEM_TYPE_RARE:
				$user_box_gacha->rare_remain--;
				break;
			case AppConstants::ITEM_TYPE_SRARE:
				$user_box_gacha->srare_remain--;
				break;
		}
		$user_box_gacha->draw_time = date('Y-m-d H:i:s');
		UserBoxGacha::update_one($user_box_gacha);
		return $user_box_gacha;
	}
}
