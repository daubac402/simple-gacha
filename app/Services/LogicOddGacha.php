<?php namespace App\Services;

use App\Services\LogicGachaBase;
use App\UserFreeGachaLog;
use App\GachaMasterType;

class LogicOddGacha extends LogicGachaBase{
	
	protected function can_draw_free($user, $gacha_type_id, $mark_draw = false)
	{
		$user_free_gacha_log = UserFreeGachaLog::find_by_keys($user->id, $gacha_type_id);
		if (isset($user_free_gacha_log))
		{
			$gacha_master_type = GachaMasterType::find($gacha_type_id);
			if (null != $gacha_master_type)
			{
				$diff_second = LogicOddGacha::get_diff_second_from_now($user_free_gacha_log->draw_time);
				if ($diff_second < $gacha_master_type->reset_draw_free_period)
				{
					return false;
				}
			}
			if ($mark_draw)
			{
				$user_free_gacha_log->draw_time = date('Y-m-d H:i:s');
				UserFreeGachaLog::update_one($user_free_gacha_log);
			}
		}
		else
		{
			if ($mark_draw)
			{
				$user_free_gacha_log = new UserFreeGachaLog(array(
					'user_id'		=> $user->id,
					'gacha_type_id'	=> $gacha_type_id,
					'draw_time'		=> date('Y-m-d H:i:s'),
				));
				$user_free_gacha_log->save();
			}
		}
		return true;
	}
}
