<?php namespace App\Services;

use App\Services\LogicBase;
use App\GachaMasterType;
use App\ItemMaster;
use App\UserItem;
use Illuminate\Support\Facades\Auth;
use App\Services\LogicUserCoin;
use DB;

class LogicGachaBase extends LogicBase {

	const SUCCESS					= 0;
	const ERROR_UNKNOWN				= 1;
	const ERROR_OUT_OF_MONEY 		= 2;
	const ERROR_OUT_OF_BOX_GACHA	= 3;

	protected $logic_user_coin;
	protected $lot_list;

	public function __construct()
	{
		$this->logic_user_coin = new LogicUserCoin();
	}

	public function draw_gacha($user, $gacha_master_type)
	{
		DB::beginTransaction();
		try
		{
			$can_draw_free_status = $this->can_draw_free($user, $gacha_master_type->gacha_type_id, true);
			$take_coin_pass = $this->take_user_coin($user, $gacha_master_type, $can_draw_free_status);
			if (!$take_coin_pass)
			{
				DB::rollback();
				$result_item['error'] = self::ERROR_OUT_OF_MONEY;
				return $result_item;
			}

			$this->lot_list = $this->make_lot_list($user, $gacha_master_type);
			$lot_result = $this->draw_lot($this->lot_list);
			if (NULL == $lot_result)
			{
				DB::rollback();
				$result_item['error'] = self::ERROR_OUT_OF_BOX_GACHA;
				return $result_item;
			}
			
			$box_gacha_info = $this->update_remain_box_gacha($user, $gacha_master_type, $lot_result);

			$result_item = $this->give_result_item($user, $lot_result);
			$result_item['draw_free'] = $can_draw_free_status;
			$result_item['box_gacha_info'] = $box_gacha_info;

			$result_item['error'] = 0;
			DB::commit();
			return $result_item;
		}
		catch (\Exception $e)
		{
			DB::rollback();
			$result_item['error'] = self::ERROR_UNKNOWN;
			return $result_item;
		}
	}

	protected function make_lot_list($user, $gacha_master_type)
	{
		return array_intersect_key($gacha_master_type['attributes'], array_flip(array_keys(GachaMasterType::LOT_TYPES)));
	}

	/**
	 * input array lot_list [X,Y,Z,...]
	 * X is an array have (name) and (probability)
	 *
	 * @return lot_result name
	 */
	protected function draw_lot($lot_list)
	{
		$rate_list = array_values($lot_list);
		$max_number = array_sum($rate_list);
		if ($max_number == 0)
		{
			return NULL;
		}
		$number = mt_rand(0, $max_number - 1);

		$hit_lot = false;
		$hit_range = array();
		$from_rate = 0;
		foreach ($lot_list as $index => $lot) 
		{
			$to_rate = $from_rate + $lot;
			$hit_range[] = array($from_rate, $to_rate);
			if ($from_rate <= $number && $number < $to_rate)
			{
				$hit_lot = $index;
				break;
			}
			else
			{
				$from_rate += $lot;
			}
		}
		return $hit_lot;
	}

	protected function give_result_item($user, $lot_type)
	{
		//Get random card from
		$item_rarity_result = GachaMasterType::LOT_TYPES[$lot_type];
		$lot_list = ItemMaster::find_by_rarity($item_rarity_result);
		$result_item = $lot_list[mt_rand(0, count($lot_list) - 1)];

		//give user_item
		$user_item = UserItem::find_by_keys($user->id, $result_item->id);
		if (isset($user_item))
		{
			$user_item->number++;
			UserItem::update_one($user_item);
			return $user_item;
		}
		else
		{
			$user_item = new UserItem(array(
				'user_id'		=> $user->id,
				'item_id'		=> $result_item->id,
				'number'		=> 1,
			));
			$user_item->save();
			return $user_item;
		}
	}

	protected function can_draw_free($user, $gacha_type_id, $mark_draw = false)
	{
		return false;
	}

	protected function update_remain_box_gacha($user, $gacha_master_type, $lot_result)
	{
		return NULL;
	}

	protected function take_user_coin($user, $gacha_master_type, $can_draw_free_status = false)
	{
		if (!$can_draw_free_status)
		{
			return $this->logic_user_coin->take_user_coin($user, $gacha_master_type->price);
		}
		return true;
	}
}
