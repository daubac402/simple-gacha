<?php namespace App;

use App\BaseModel;

class ItemMaster extends BaseModel {

	public static function find_by_rarity($rarity)
	{
		if (is_array($rarity))
		{
			return ItemMaster::whereIn('rarity', $rarity)->get();
		}
		else
		{
			return ItemMaster::where('rarity', '=', $rarity)->get();
		}
	}

}
