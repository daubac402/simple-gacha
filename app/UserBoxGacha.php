<?php namespace App;

use App\BaseModel;

class UserBoxGacha extends BaseModel {

	public $timestamps = true;
	protected $fillable = [
		'user_id', 
		'gacha_type_id', 
		'common_remain', 
		'uncommon_remain', 
		'rare_remain', 
		'srare_remain', 
		'draw_time'
		];

	public static function find_by_keys($user_id, $gacha_type_id)
	{
		if (is_array($gacha_type_id))
		{
			return UserBoxGacha::where('user_id', '=', $user_id)->whereIn('gacha_type_id', $gacha_type_id)->get();
		}
		else
		{
			return UserBoxGacha::where('user_id', '=', $user_id)->where('gacha_type_id', '=', $gacha_type_id)->first();
		}
	}

	public static function update_one($user_box_gacha)
	{
		$user_box_gacha->updated_at = date('Y-m-d H:i:s');
		return UserBoxGacha::where('user_id', '=', $user_box_gacha->user_id)->where('gacha_type_id', '=', $user_box_gacha->gacha_type_id)->update($user_box_gacha['attributes']);
	}

}
