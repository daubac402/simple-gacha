<?php namespace App;

use App\BaseModel;

class UserItem extends BaseModel {

	public $timestamps = true;
	protected $fillable = ['user_id', 'item_id', 'number'];

	public static function find_by_keys($user_id, $item_id)
	{
		if (is_array($item_id))
		{
			return UserItem::where('user_id', '=', $user_id)->whereIn('item_id', $item_id)->get();
		}
		else
		{
			return UserItem::where('user_id', '=', $user_id)->where('item_id', '=', $item_id)->first();
		}
	}

	public static function update_one($user_item)
	{
		$user_item->updated_at = date('Y-m-d H:i:s');
		return UserItem::where('user_id', '=', $user_item->user_id)->where('item_id', '=', $user_item->item_id)->update($user_item['attributes']);
	}
}
