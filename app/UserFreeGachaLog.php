<?php namespace App;

use App\BaseModel;

class UserFreeGachaLog extends BaseModel {

	public $timestamps = true;
	protected $fillable = ['user_id', 'gacha_type_id', 'draw_time'];

	public static function find_by_keys($user_id, $gacha_type_id)
	{
		if (is_array($gacha_type_id))
		{
			return UserFreeGachaLog::where('user_id', '=', $user_id)->whereIn('gacha_type_id', $gacha_type_id)->get();
		}
		else
		{
			return UserFreeGachaLog::where('user_id', '=', $user_id)->where('gacha_type_id', '=', $gacha_type_id)->first();
		}
	}

	public static function update_one($user_free_gacha_log)
	{
		$user_free_gacha_log->updated_at = date('Y-m-d H:i:s');
		return UserFreeGachaLog::where('user_id', '=', $user_free_gacha_log->user_id)->where('gacha_type_id', '=', $user_free_gacha_log->gacha_type_id)->update($user_free_gacha_log['attributes']);
	}

}
