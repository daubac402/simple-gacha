<?php

use App\ItemMaster;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ItemMastersTableSeeder extends Seeder {

    public function run()
    {
    	ItemMaster::truncate();

    	$item_composits = array(
    		'Energy Pack' 			=> 'Restore your HP', 
    		'Power Pack'			=> 'Restore your Attack Power',
    		'Flare Stick'			=> 'Give you two random items',
    		'Shield Barrier'		=> 'Prevents one attack targeting your Resource',
    		'RDS Bar'				=> 'Recharge your Raid energy',
    		'GPS Radar'				=> 'Find a truck for you in Raid event',
    		'Odin Decree'			=> 'Increase your card mastery',
    		'Black Powder'			=> 'Kick a card from the black slot of your opponent',
    		'Golden Ring'			=> 'Protect your card from damage in one turn',
    		'Power Treads'			=> 'Boost a specific attribute by 8 points',
    		'Black King Bar'		=> 'Can be target by spell in 10 seconds',
    		'Hand of Midas'			=> 'Get more coins from destroying cards',
    	);
    	for ($composit_id = 0; $composit_id < count($item_composits); $composit_id++) {
    		for ($i=0; $i < 10; $i++) {
    			$name_tmp = array_keys($item_composits)[$composit_id];
    			$desc_tmp = array_values($item_composits)[$composit_id];
	    		ItemMaster::create(array(
		        	'id' 			=> $composit_id * 10 + $i + 1,
		        	'name' 			=> "{$name_tmp} - {$i} Star",
		        	'description' 	=> $desc_tmp,
		        	'rarity' 		=> floor($i / 2.5) + 1,
		        	'created_at'	=> date('Y-m-d H:i:s'),
		        	'updated_at'	=> date('Y-m-d H:i:s'),
	        	));
	    	}
    	}
    }

}