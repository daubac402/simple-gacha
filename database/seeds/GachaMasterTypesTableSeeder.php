<?php

use App\GachaMasterType;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class GachaMasterTypesTableSeeder extends Seeder {

    public function run()
    {
        GachaMasterType::truncate();

        GachaMasterType::create(array(
        	'gacha_type_id' => 1,
        	'name' 			=> 'Normal Gacha',
        	'description' 	=> 'Common 70% - Uncommon 25% - Rare 4% - Super Rare 1%',
            'price'         => 100,
            'reset_draw_free_period'     => 3600, // 1 hour
            'reset_box_gacha_time'       => NULL, // do not apply
        	'common_prob' 	=> 7000,
        	'uncommon_prob' => 2500,
        	'rare_prob' 	=> 400,
        	'srare_prob' 	=> 100,
        	'created_at'	=> date('Y-m-d H:i:s'),
        	'updated_at'	=> date('Y-m-d H:i:s'),
        	));
        GachaMasterType::create(array(
        	'gacha_type_id' => 2,
        	'name' 			=> 'Expensive Gacha',
        	'description' 	=> 'Common 10% - Uncommon 50% - Rare 30% - Super Rare 10%',
            'price'         => 1000,
            'reset_draw_free_period'     => 86400, // 1 day
            'reset_box_gacha_time'       => NULL, // do not apply
        	'common_prob' 	=> 1000,
        	'uncommon_prob' => 5000,
        	'rare_prob' 	=> 3000,
        	'srare_prob' 	=> 1000,
        	'created_at'	=> date('Y-m-d H:i:s'),
        	'updated_at'	=> date('Y-m-d H:i:s'),
        	));
        GachaMasterType::create(array(
        	'gacha_type_id' => 3,
        	'name' 			=> 'Box Gacha',
        	'description' 	=> 'Common 55 items - Uncommon 25 items - Rare 15 items - Super Rare 5 items',
            'price'         => 500,
            'reset_draw_free_period'     => 0, // do not apply
            'reset_box_gacha_time'       => '000000', //at 0h0m0s
        	'common_prob' 	=> 55,
        	'uncommon_prob' => 25,
        	'rare_prob' 	=> 15,
        	'srare_prob' 	=> 5,
        	'created_at'	=> date('Y-m-d H:i:s'),
        	'updated_at'	=> date('Y-m-d H:i:s'),
        	));
    }

}