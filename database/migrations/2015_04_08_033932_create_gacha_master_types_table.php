<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGachaMasterTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gacha_master_types', function(Blueprint $table)
		{
			$table->integer('gacha_type_id');
			$table->string('name', 30);
			$table->string('description', 500)->nullable();
			$table->integer('price');
			$table->integer('reset_draw_free_period')->nullable();
			$table->string('reset_box_gacha_time')->nullable();
			$table->integer('common_prob');
			$table->integer('uncommon_prob');
			$table->integer('rare_prob');
			$table->integer('srare_prob');
			$table->timestamps();

			$table->primary('gacha_type_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gacha_master_types');
	}

}
