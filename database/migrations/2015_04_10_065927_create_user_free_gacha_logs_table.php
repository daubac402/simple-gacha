<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFreeGachaLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_free_gacha_logs', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->integer('gacha_type_id');
			$table->dateTime('draw_time');
			$table->timestamps();

			$table->primary(['user_id', 'gacha_type_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_free_gacha_logs');
	}

}
