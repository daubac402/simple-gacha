<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBoxGachasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_box_gachas', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->integer('gacha_type_id');
			$table->integer('common_remain');
			$table->integer('uncommon_remain');
			$table->integer('rare_remain');
			$table->integer('srare_remain');
			$table->dateTime('draw_time')->nullable();
			$table->timestamps();

			$table->primary(['user_id', 'gacha_type_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_box_gachas');
	}

}
