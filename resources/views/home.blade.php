@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			@foreach ($gacha_master_types as $gacha_master)
			    <div class="panel panel-default">
				<div class="panel-heading">{{ $gacha_master->name }}

						<div class="panel-heading-description" id="desc_{{ $gacha_master->gacha_type_id }}">
							@if ($gacha_master->reset_box_gacha_time == NULL)
								{{ $gacha_master->description }}
							@endif
						</div>
				</div>
				<div class="panel-body">
					<p id="result_{{ $gacha_master->gacha_type_id }}">Result here</p>
					<button class="draw_btn" id="{{ $gacha_master->gacha_type_id }}">Draw</button>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>

<script>
	function do_ajax(draw_btn_id)
	{
		var url_adr = "{{ url('/home') }}" + "/draw_gacha_" + draw_btn_id;
		$.ajax({
			url: url_adr,
			type: "POST",
			data: {
				_token : "{{ csrf_token() }}"
			},
			beforeSend: function(data)
			{

			},
			success: function(data)
			{
				try {
			        re = JSON.parse(data);
					$("#result_" + draw_btn_id).html(re.message);
					if (null != re.user_current_coin_update)
					{
						$("#user_coin").html(re.user_current_coin_update);
					}
					if (null != re.box_gacha_desc)
					{
						$("#desc_" + draw_btn_id).html(re.box_gacha_desc);
					}
			    } catch (e) {
			        $("#result_" + draw_btn_id).html('<span style="color:red">Error parsing JSON</span>');
			    }
			},
			complete: function(data)
			{

			},
			error: function (err) {
		    	$("#result_" + draw_btn_id).html('<span style="color:red">Error!</span>');
		    },
		});
	};

	$(document).ready(function(){
	    $(".draw_btn").click(function(){
	    	do_ajax($(this).attr('id'));
	    });
	});
</script>
@endsection
